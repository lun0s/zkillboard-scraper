#!/usr/bin/python3.6
import json, urllib.request, time, dateutil.parser, configparser
from pg import DB

config = configparser.ConfigParser()
config.read('config.ini')
db = DB(dbname=config['pg']['db'], host=config['pg']['host'], port=int(config['pg']['port']), user=config['pg']['username'], passwd=config['pg']['password'])

while True:
  reDisQ = urllib.request.urlopen("https://redisq.zkillboard.com/listen.php?ttl=5&queueID=zKillScraper647")
  reDisQ = reDisQ.read().decode('UTF-8')
  reDisQ = json.loads(reDisQ) #JSON of provided killmail
  if reDisQ['package'] is None: #Finished retrieving killmails
    break #Loop will terminate when no killmail is provided

  #Below values should always be present
  killID = reDisQ['package']['killID']
  fittedValue = reDisQ['package']['zkb']['fittedValue']
  totalValue = reDisQ['package']['zkb']['totalValue']
  isSolo = reDisQ['package']['zkb']['solo']
  isNPC = reDisQ['package']['zkb']['npc']
  solarSystemID = reDisQ['package']['killmail']['solar_system_id']
  killmailTime = reDisQ['package']['killmail']['killmail_time'] #ISO 8601 formatted
  shipTypeID = reDisQ['package']['killmail']['victim']['ship_type_id']
  damageTaken = reDisQ['package']['killmail']['victim']['damage_taken']
  victimCorpID = reDisQ['package']['killmail']['victim']['corporation_id']
  itemsJSON = json.dumps(reDisQ['package']['killmail']['victim']['items'])
  fittedValue = reDisQ['package']['zkb']['fittedValue']
  totalValue = reDisQ['package']['zkb']['totalValue']
  numAttackers = 0
  attackersJSON = json.dumps(reDisQ['package']['killmail']['attackers'])
  
  #Below values  aren't consistently available
  try:
    victimID = reDisQ['package']['killmail']['victim']['character_id']
  except:
    victimCorpID = reDisQ['package']['killmail']['victim']['corporation_id']
    victimID = 0
  try:
    victimAllianceID = reDisQ['package']['killmail']['victim']['alliance_id']
  except:
    victimAllianceID = 0
  
  db.query(f'INSERT INTO "killStorage"("killID","killmailTime", "solarSystemID", "victimID", "victimCorpID", "victimAllianceID", "attackersJSON", "fittedValue", "totalValue", "damageTaken", "isSolo", "isNPC", "itemsJSON", "victimShipID") VALUES (\'{killID}\',\'{killmailTime}\', \'{solarSystemID}\', \'{victimID}\', \'{victimCorpID}\', \'{victimAllianceID}\', \'{attackersJSON}\', \'{fittedValue}\', \'{totalValue}\', \'{damageTaken}\', \'{isSolo}\', \'{isNPC}\', \'{itemsJSON}\', \'{shipTypeID}\')')  